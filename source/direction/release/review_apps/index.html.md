---
layout: markdown_page
title: "Category Direction - Review Apps"
---

- TOC
{:toc}

## Review Apps

Review Apps let you build a review process right into your software development workflow
by automatically provisioning test environments for your code, integrated right
into your merge requests.

This area of the product is in need of continued refinement to add more kinds of
review apps (such as for mobile devices), and a smoother, easier to use experience.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AReview%20Apps)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)
- [Documentation](https://docs.gitlab.com/ee/ci/review_apps/)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1299) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

We recently conducted research regarding the Review Apps experience and have gained a lot of insights
 of what can make this experience better. These can be followed under the following [public epic](https://gitlab.com/groups/gitlab-org/-/epics/2251).
 If you did not get a chance to participate in the interviews but still feel that you have a lot to say, please
 leave us feedback in the issues or contribute new ones.

Some improvements that we are currently working on are:

 * We understand that environments tend to grow exponentially and it is a burden to maintain them. We will make it easier to clean up
  environments with ([gitlab#19724](https://gitlab.com/gitlab-org/gitlab/issues/19724)).
 * Improving the initial setup experience can be followed under the following [epic](https://gitlab.com/groups/gitlab-org/-/epics/2349), 
  as an initial step, we will create a button that will provide a snippet to be copied to
  your `.gitlab-ci.yml` file that will configure Review apps [gitlab#118844](https://gitlab.com/gitlab-org/gitlab/issues/118844).  


## Maturity Plan

This category is currently at the "Complete" maturity level, and
our next maturity target is Lovable (see our [definitions of maturity levels](/direction/maturity/#maturity-plan)).
Key deliverables to achieve this are:
- [Extend the API to provide an ability to run scripts to delete environment](https://gitlab.com/gitlab-org/gitlab/issues/5582) (Complete)
- [Mechanism to clean up stale environments](https://gitlab.com/gitlab-org/gitlab/issues/19724)
- [Optional expiration time for environments](https://gitlab.com/gitlab-org/gitlab/issues/20956)
- [One button to enable review apps, auto-edit `.gitlab-ci.yml`, auto-configure GKE](https://gitlab.com/groups/gitlab-org/-/epics/2349)
- [Truly dynamic environment URLs](https://gitlab.com/gitlab-org/gitlab/issues/17066)
- [First-class review apps](https://gitlab.com/gitlab-org/gitlab/issues/18982)

## Competitive Landscape

One big advantage Heroku Review Apps have over ours is that they are easier to set up
and get running. Ours require a bit more knowledge and reading of documentation to make
this clear. We can make our Review Apps much easier (and thereby much more visible) by
implementing [One button to enable review apps, auto-edit `.gitlab-ci.yml`, auto-configure GKE](https://gitlab.com/groups/gitlab-org/-/epics/2349),
which does the heavy lifting of getting them working for you.

## Top Customer Success/Sales Issue(s)

Management of Review Apps can be a challenge, particularly in cleaning them up. To
highlight the severity of how this issue can grow, www-gitlab-com project has over
1,500 running stale environments at the time of writing this, with no clear easy way
to clean them up. There are two main items that look to address this challenge:

- [gitlab#20956](https://gitlab.com/gitlab-org/gitlab/issues/20956) builds in an expiration date for review apps, beyond which they will automatically be terminated.
- [gitlab#19724](https://gitlab.com/gitlab-org/gitlab/issues/19724) (also the #2 customer issue) implements a way to clean up environments that either did not have an expiration date or were not terminated for other reasons.

## Top Customer Issue(s)

The top customer issue impacting users of Review Apps is [gitlab#20956](https://gitlab.com/gitlab-org/gitlab/issues/20956)
which sets an expiration date for an environment. 

## Top Internal Customer Issue(s)

In some cases after a review app is not available for a specific Merge Request  
([gitlab#10733](https://gitlab.com/gitlab-org/gitlab/issues/10733)), adding some 
information to the user as to what action needs to be done in order to resolve this and
taker action directly from the MR itself, would help not only our customers, 
but also our own developers. 

## Top Vision Item(s)

Our focus for the vision is to bring Review Apps to mobile workflows via
[gitlab#20295](https://gitlab.com/gitlab-org/gitlab/issues/20295) - adding
support to Android/iOS emulators via the Review App will enable a whole new kind
of development workflow in our product, and make Review Apps even more valuable.
