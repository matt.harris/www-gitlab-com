---
layout: markdown_page
title: "FY21-Q1 OKRs"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from February 1, 2020 to April 30, 2020.

## On this page
{:.no_toc}

- TOC
{:toc}

### 1. CEO: IACV
1. CEO KR: End Q1 with Q2 New business IACV Stage 3+ [Pipeline](/handbook/marketing/revenue-marketing/#revenue-marketing-kpi-definitions) and Growth business IACV Stage 3+ Pipeline both ahead of plan.
    1. **CRO: Build strong FY21 pipeline.**    
        1. CRO KR: FY21-Q1 pipeline at 2X target (stage 3+)
        1. CRO KR: FY21-Q2 pipeline at 1.2X target (stage 3+)
            1. **VP, Field Ops: Instrument systems and programs to intended targets.**
                1. VP Field Ops KR: Factor pipeine targets to relevant tools.
                    1. **Sr Director of Sales Operations: Assess solution for pipeline targets.**
                        1. Sr Director of Sales Operations KR: Implement targets into Clari (or SFDC).
                        1. Sr Director of Sales Operations KR: Measure effectiveness to 90% accuracy.
                    1. **Sr Manager of Sales Strategy: Measure and report on iACV forecast accuracy and take action to improve**
                        1. Sr Manager of Sales Strategy KR: Assess Clari abilities to trend or track in Periscope, assess rep/mgr level forecast accuracy to 100% throughout the QTR and identify actions for sales managers
    1. **CRO: Launch New Channel Program** 
       1. CRO KR: New tiers, terms and agreement complete, published on partner portal and handbook for all partners
       1. CRO KR: Automated deal reg launched and available in SFDC Partner Portal 
       1. CRO KR: Deliver at least one new PIO in every region through SFDC Partner Portal
    1. **CRO: Increase stage usage by at least one stage for 10 of top 50 accounts with only 1-2 stages in use as measured by SMAU >10% per stage.** 
       1. CRO KR: Targeted account based plan to offer services and support to utilize a new stage at each of the 50 targeted accounts.
       1. CRO KR: 10 accounts add at least one stage with SMAU moving from <5% to >25% in a specific stage.
1. CEO KR: Maintain quota capacity at least 5% ahead of plan for FY21.  Exceed Q1 [NQR](/handbook/sales/commissions/#quotas-overview) [hiring](/handbook/hiring/charts/sales-nqr/)) by at least 5% quota coverage.
    1. **CRO: Exceed hiring plan for NQR hires for Q1**
        1. CRO KR: develop agreed upon quarterly targets and developed reporting on QTD performance.
        1. CRO KR: establish SLA’s for Hiring Teams around interview and feedback turnaround. Assign DRI’s for vacant leadership roles to ensure minimal pause of hiring to plan.
    1. **CRO: At or above plan for OQR hires for Q1** 
        1. CRO KR: develop agreed upon quarterly targets and developed reporting on QTD performance.
        1. CRO KR: establish SLA’s for Hiring Teams around interview and feedback turnaround. Assign DRI’s for vacant leadership roles to ensure minimal pause of hiring to plan.
    1. **CRO: Implement Phase 2 of the 5C Sales Talent Model to allow for raising the bar with hiring talent for NQR and OQR roles.**
        1. CRO KR: Initiate phase 2 of the 5C Sales Talent Model and expand to all NQR and OQR roles. 
        1. CRO KR: 100% of applicants that make it to team interview have gone through the 5C Sales Talent Model Interview process. 
1. CEO KR: Renewal process satisfaction, as measured by [SSAT](/handbook/business-ops/data-team/kpi-index/#satisfaction) for tickets about renewals, is greater than 95%.
    1. **Director of Product, Growth: Improve the customer experience with billing related workflows.** [gitlab-com&263](https://gitlab.com/groups/gitlab-com/-/epics/263)
        1. Director of Product, Growth KR: Make material improvements to direct signup, trial, seat true-up, upgrade, and renewal workflows, both for .com and self-hosted customers. [gitlab-com/Product#725](https://gitlab.com/gitlab-com/Product/issues/725)
        1. Director of Product, Growth KR: Drive [Support Satisfaction](/handbook/support/performance-indicators/#support-satisfaction-ssat) scores, filtered for billing, to >95%. [gitlab-com/Product#726](https://gitlab.com/gitlab-com/Product/issues/726)
        1. Director of Product, Growth KR: Drive [$561k in incremental IACV](https://gitlab.com/gitlab-org/growth/product/issues/805). [gitlab-com/Product#727](https://gitlab.com/gitlab-com/Product/issues/727)
   1. **VP of Product Management: Complete pricing analysis and, if required, roll out related updates to GitLab's pricing and packaging.** [gitlab-com&264](https://gitlab.com/groups/gitlab-com/-/epics/264)
        1. VP of Product Management KR: Complete pricing analysis project and, if required, implement at least the first phase of any recommended changes. [gitlab-com/Product#728](https://gitlab.com/gitlab-com/Product/issues/728)
   1. **EVP of Engineering:** [Enterprise-grade dot com](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6375)
        1. EVP of Engineering KR: Launch transparent weighted appdex measure of availability
        1. EVP of Engineering KR: 99.95% availability
        1. EVP of Engineering KR: Deliver the top 10 customer requested features as prioritized by Product Management
    1. **Sr Director of Development:** [Support incremental IACV](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6237)
        1. Sr Director of Development KR: Deliver the top 10 customer requested features as prioritized by Product Management
        1. **Director of Engineering, Dev:** [Support incremental IACV](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6243)
            1. Director of Engineering, Dev KR: Deliver the top 2 customer-requested features as prioritized by Product Management in each stage
            1. Director of Engineering, Dev KR: Deliver 15 items in the performance or Infra/Dev dashboard
        1. **Director of Engineering, Enablement:** [Continue efforts to build enterprise-grade SaaS](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6205)
            1. Director of Engineering, Enablement KR: Complete dogfooding Geo on GitLab.com staging environment [gitlab-org&575](https://gitlab.com/groups/gitlab-org/-/epics/575)
            1. Director of Engineering, Enablement KR: Enable ElasticSearch for paid groups on GitLab.com [gitlab-org&1736](https://gitlab.com/groups/gitlab-org/-/epics/1736)
            1. Director of Engineering, Enablement KR: Scope and start MVC of database partitioning, related epic [gitlab-org&2023](https://gitlab.com/groups/gitlab-org/-/epics/2023), completion of MVC may extend into Q2 due to dependency of PostgreSQL 11 upgrade.
        1. **Director of Engineering, Defend:** [3 new Defend features go from planned to minimal](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6192)
            1. Director of Engineering, Defend KR: 3 new Defend MVC features go from planned to minimal, per PM priorities
            1. Director of Engineering, Defend KR: Track all planning priority issues and make sure they are being completed on time
        1. **Director of Engineering, Growth:** [Deliver Improved product usage data](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6240)
            1. Director of Engineering, Growth KR: Implement Telemetry Strategy
            1. Director of Engineering, Growth KR: Support repeatable experiments across gitlab.com and the customers application
            1. Director of Engineering, Growth KR: Track all planning priority issues and make sure they are being completed on time
    1. **Director of Quality:** [Ensure enterprise grade readiness by improving test coverage, reference architectures and test stability](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6330)
        1. Director of Quality KR: Close out 19 remaining enterprise end-to-end test gap coverage.
        1. Director of Quality KR: Design and roll out department official on-call process based on existing pipeline triage rotation. 
        1. **Enablement QE:** [Produce AWS equivalent reference architectures and increase performance test coverage](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6289)
            1. Enablement QE KR: Create the first iteration of Kubernetes/Omnibus 50K reference architecture.
            1. Enablement QE KR: Deliver AWS-equivalent configurations for the 5 existing reference architectures.
            1. Enablement QE KR: Improve coverage to performance test suite, add 5 new controllers.
        1. **Dev QE:** [Create a reliable end-to-end test suite to guard every deploy in the release process](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6331)
            1. Dev QE KR: Define method and process to promote an end-to-end test into reliable state.
            1. Dev QE KR: Create a reliable end-to-end test suite, achieve 50% of total end-to-end tests, run against staging as part of every deployment.
            1. Dev QE KR: Improve format of test reports in 3 key areas of the release process, master, staging and canary.
    1. **Director of Customer Support:** [Develop, educate and rollout ticket deflection approach](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6227)
        1. Director of Customer Support KR: Clearly defined process to deliver known solutions to customers through initial Support portal contact.
        1. Director of Customer Support KR: Training developed and delivered to all Support and available to relevant teams in Gitlab.
        1. Director of Customer Support KR: Define method to measure Support Engineer contribution as well as impact to initial ticket creation/deflection.
    1. **VP of Security:** [Gearing ratio for H1 bounties](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5903)
        1. VP of Security KR: Get exec sign-off in weekly PM&Eng meeting for new gearing ratios.
        1. VP of Security KR: Implement in H1.
        1. VP of Security KR: Evaluate at EOQ and implement new output, if necessary.
    1. **Interim VP of Infrastructure:** [Enterprise-grade dot com](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6219)
        1. Interim VP of Infrastructure KR: Launch transparent weighted appdex measure of availability
        1. Interim VP of Infrastructure KR: 99.95% availability
        1. Interim VP of Infrastructure KR: Identify cost-savings measures and implement 3 TBD changes

### 2. CEO: Popular next generation product
1. CEO KR: [SMAU](/handbook/product/metrics/#stage-monthly-active-users-smau) is being measured and reported in 100% of Sales and Product [Key Meetings](/handbook/finance/key-meetings/).
    1. **Director of Product, Growth:  Ensure accurate data collection and reporting of AMAU and SMAU metrics.** [gitlab-com&265](https://gitlab.com/groups/gitlab-com/-/epics/265)
        1. Director of Product, Growth KR: Deliver AMAU and SMAU tracking with less than 2% uncertainty. [gitlab-com/Product#729](https://gitlab.com/gitlab-com/Product/issues/729)
1. CEO KR: [SPU](/handbook/product/metrics/#stages-per-user) increases by 0.25 stages from EOQ4 to EOQ1.
1. CEO KR: [MAU](/handbook/product/metrics/#monthly-active-users-mau) increases 5% from EOQ4 to EOQ1.
1. **VP of Product Management: Proactively validate problems and solutions with customers.** [gitlab-com&266](https://gitlab.com/groups/gitlab-com/-/epics/266)
    1. VP of Product Management KR: At least 2 validation cycles completed per Product Manager. [gitlab-com/Product#730](https://gitlab.com/gitlab-com/Product/issues/730)
1. **VP of Product Management: Create demos of the top competitor in each stage to compare against our own demos completed in Q4.** [gitlab-com&267](https://gitlab.com/groups/gitlab-com/-/epics/267)
    1. VP of Product Management KR: Deliver one recorded demo for each stage. [gitlab-com/Product#731](https://gitlab.com/gitlab-com/Product/issues/731)
1. **Principal Product Manager, Product Operations: Roll out [Net Promoter Score (NPS)](/handbook/product/metrics/#paid-net-promoter-score) tracking.** [gitlab-com&268](https://gitlab.com/groups/gitlab-com/-/epics/268)
    1. Principal Product Manager, Product Operations KR: Survey at least 25% of GitLab's paid customer base, with a reponse rate of >4%. [gitlab-com/Product#732](https://gitlab.com/gitlab-com/Product/issues/732)
1. **Director, Product, Ops:  Work with GitLab's infrastructure team to dogfood our APM metrics.** [gitlab-com&269](https://gitlab.com/groups/gitlab-com/-/epics/269)
    1. Director, Product, Ops KR:  Ensure APM metric dogfooding is properly prioritized. [gitlab-com/Product#733](https://gitlab.com/gitlab-com/Product/issues/733)
1. **VP of Product Strategy: Get strategic thinking into the org.** 
    1. VP of Product Strategy KR: Secure and Enablement section strategy reviews
    1. VP of Product Strategy KR: Produce [strategy visuals](https://gitlab.com/gitlab-com/Product/issues/512)
1. **VP of Product Strategy: Lay groundwork for strategic initiatives.** 
    1. VP of Product Strategy KR: Hire strategic initiatives team
1. **Sr. Director of Corp Dev: Get acquisitions into shape; build a well-oiled machine.** 
    1. Sr. Director of Corp Dev KR: Identify 1000 [qualified acquisition targets](/handbook/acquisitions/performance-indicators/#qualified-acquisition-targets).
1. **EVP of Engineering:** [Efficiently build our product vision](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6376)
    1. Key result: Increase MR Rate from topline goal of 10 to 11
    1. Key result: Dogfood self-monitoring on dot com
    1. **Director of UX:** [Increase the value of category maturity ratings by validating them with users](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6201)
        1. Key result: Validate category maturity ratings with users for all categories that moved to the next rating (except Minimal, which is self assigned) during Q4 FY20.
    1. **Director of UX:** [Empower Product Managers and Designers to independently conduct UX Scorecard validation](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6203)
        1. Key result: Update UX handbook with helpful context and tips, linking to existing research training where applicable.
    1. **Sr Director of Development:** [Deliver on our product vision by being more iterative](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6230)
        1. Key result: Reduce Average Review to Merge Time by 1 day
        1. Key result: Increase MR Rate from topline goal of 10 to 11
        1. Key result: Based on Started to Shipped % measure improve team performance
        1. **Director of Engineering, Dev:** [Improve section productivity](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6241)
            1. Key result: Overall section MR/engineer Rate increases to >=11 by the end of April.
            1. Key result: Sections Mean time to merge below 12 days
            1. Key result: Reduce Onboarding Time by Creating a Development Bootcamp video series
        1. **Director of Engineering, Defend:** [Accelerate defend productivity](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6193)
            1. Key result: >=1 defend engineer volunteers becomes a trainee maintainer and starts the process to become one by the end of the quarter
            1. Key result: Maintain current developers to maintainers ratio - Nominate and add maintainers as we are increasing (BackEnd increase from X to Y, Frontend increase by X to Y)
            1. Key result:  Fully document and where possible fully automate both creation and validation of defend back-end development (autodevops with local GDK) environment by the end of the quarter
        1. **Director of Engineering, Secure:** [Increase Productivity](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6198)
            1. Key result: Increase rolling 6 month average by >=10%
        1. **Director of Engineering, CI/CD:** [Improve monthly MR Rate](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6234)
            1. Key result: Overall section MR Rate increases to 10 by the end of April
            1. Key result: Improve Say/Do to 70%, measured by Deliverable items delivered in each milestone
            1. Key result: Increase rolling 6 month average by >=10%
        1. **Director of Engineering, Ops:** [Dogfood Monitoring](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6207)
            1. Key result: Identify with stakeholders (e.g. PM) key dogfooding metrics
            1. Key result: Define engineering plan to drive dogfooding of APM features
            1. Key result: Target an improvement in key dogfooding metric(s)
        1. **Director of Engineering, Ops:** [Increase Section MR Rate](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6208)
            1. Key result: Section MR Rate increases 20% month over month through April.
        1. **Director of Engineering, Enablement:** [Improve monthly MR Rate](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6204)
            1. Key result: Overall section MR Rate increases to 10~12 by the end of April.
            1. Key result: Track all Deliverable issues and achieve 60~70% delivery rate in each release.
            1. Key result: Keep BE maintainer ratio 8~9:1.
        1. **Director of Engineering, Growth:** [Improve section productivity](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6239)
            1. Key result: Overall section MR Rate increases from approx 9 to 11 by the end of April.
            1. Key result: Achieve FE and BE maintainer ratio 7:1.
            1. Key result: At least one deliverable per week, per growth group (48 in total).
   1. **Director of Quality:** [Improve engineering efficiency and productivity by maturing pipeline performance and department metrics](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6328)
       1. Director of Quality KR: Create 3 performance indicators for defects in product facing work (creation rate, closure rate, remaining) and 2 performance indicators for velocity (release post and cycle time).
       1. Director of Quality KR: Reduce average merge request pipeline from 72.3 minutes to 45 minutes (ultimate goal of 30 minutes).
       1. Director of Quality KR: Create performance indicator to measure test planning across all product stages, capture [Quad Planning](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6318) effort in 12 stages.
       1. **Engineering Productivity:** [Increase efficiency and productivity by increasing the performance and stability of our CI pipeline](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6326)
            1. Key Result: Decrease average merge request pipeline duration from 72.3 minutes to 45 minutes (ultimate goal of 30 minutes).
            1. Key Result: Increase master pipeline success rate from 77% to 90% (ultimate goal of 99%).
            1. Key Result: Create 2 performance indicators in periscope for release post rate and cycle time.
    1. **VP of Security:** [Improve GitLab Security resilience and availability by creating independent GitLab Security environments](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6217)
        1. Key result: Migrate all existing GitLab Security tooling and infrastructure to the new environments.
        1. Key result: Full adoption of a dedicated GitLab instance for tracking of security issues within the new environments.
    1. **Interim VP of Infrastructure:** [Dogfood ops stage features](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6377)
        1. Key result: Dogfood GitLab.com uptime as part of the product (i.e., ops.gitlab.net/uptime/)
        1. Key result: Monitor our monitoring: SLOs and SLAs.
            

### 3. CEO: Great team
1. CEO KR: Teams are working [handbook-first](/handbook/handbook-usage/#why-handbook-first) and no content is being produced in other tools, e.g. Docs, Classroom, etc.
1. CEO KR: There 7 certifications online for all community members to pursue.
1. CEO KR: There are 200 certifications completed by [middle managers](/company/team/structure/#middle-management) at GitLab.
1. **Director of People Operations**: Support an environment where all team members feel they belong, are valued, and can contribute; understand the key drivers for team member engagement and continuously improve
    1. Director of People Operations KR: [Roll out first iteration of Manager Toolkit](/people-group/people-group-senior-leader-priorities/issues/2)
    1. Director of People Operations KR: Roll out four training modules, two for D&I (one being Unconscious Bias) and two for L&D (Giving Feedback / Receiving Feedback)  
    1. Director of People Operations KR: Four certification modules rolled out by end of quarter  
1. **Director of People Operations**: Team members understand the value of their compensation, benefits, and perks
    1. Director of People Operations KR: 100% of team members trained on our compensation philosophy and training materials available to our wider community
    1. Director of People Operations KR: Ensure competitive benefits offerings where we have entities/PEOs. (Based on life insurance, medical insurance, and retirement)
1. **Director of People Operations**: Improve onboarding to ensure optimal levels of new team member experience and productivity
    1. Director of People Operations KR: Increase [OSAT](/people-group/people-group-metrics/#onboarding-satisfaction-osat) response rate and achieve score of 4.4 or higher (higher than target)
    1. Director of People Operations KR: Revamp [buddy program](https://gitlab.com/gitlab-com/people-group/General/issues/588#note_277801283)
    1. Director of People Operations KR: 100% of People Ops team trained to effectively handle team member questions 
1. **VP of Recruiting**: Continue to drive towards the target of [Candidates Sourced by Recruiting Department vs. Candidates Hired](/handbook/hiring/metrics/#candidates-sourced-by-recruiting-department-vs-candidates-hired) with focus on diversity, location factor and speed.
    1. VP of Recruiting KR: Achieve  ≥ 20% Candidates [Sourced by Recruiting Department vs. Candidates Hired](/handbook/hiring/metrics/#candidates-sourced-by-recruiting-department-vs-candidates-hired)
    1. VP of Recruiting KR: Sourcing sessions held for ≥90% of the open roles.
    1. VP of Recruiting KR: Prioritize diversity sourcing for 100% of the Sourcing Sessions by using diversity sourcing strings and sourcing knowledge base.
1. **VP of Recruiting** Deliver on the ambitious hiring plan, in partnership with leaders, while ensuring a unique and exceptional candidate experience for candidate pools that are diverse and representative of our communities.
    1. VP of Recruiting KR: Achieve an average location factor of .65 or below for new hires in Q1 FY21 through recruiting and sourcing efforts.
    1. VP of Recruiting KR: Accomplish [>90%](/handbook/hiring/metrics/#hires-vs-plan) of hiring plan for Q1.
    1. VP of Recruiting KR: Average Apply to Offer Accept @ 40 days (or less), excluding Director + roles.
1. **EVP of Engineering:** [Lower average location factor and enable entry-level hiring](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6374)
    1. Key result: Hire 4 interns for our pilot program
    1. Key result: Collaborate with data team on average location factor charts for each department/section head
    1. Key result: Hire VP of Infrastructure
    1. Key result: Deliver 4 career matrices as part of PlatoHQ program in response to CultureAmp feedback
    1. **Director of UX:** [Provide clear career paths for everyone in UX](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6081)
        1. Key result: Create and update Technical Writing roles
        1. Key result: Create and update UX Research roles
        1. Key result: Create and update Product Design roles
    1. **Sr Director of Development:** [Build a world class software team](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6231)
        1. Key result: Meet Q1 Hiring plan for development
        1. Key result: Implement at least one Culture Amp action item
        1. Key result: Have Senior Manager plans in place and at least X positions open
        1. **Director of Engineering, Dev:** [Hire and Scale section to target](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6242)
            1. Key result: Hire according to plan
            1. Key result: Introduce and hire at least 1 senior manager in Q1
        1. **Director of Engineering, Secure:** [Hiring to Target](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6197)
        1. **Director of Engineering, Defend:** [Hiring to Target](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6191)
            1. Key result: Fill remaining open positions for defend team (7 new hires)
        1. **Director of Engineering, CI/CD:** [Accomplish Q1 hiring goals](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6235)
        1. **Director of Engineering, Ops:** [Accomplish Q1 hiring goals](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6206)
        1. **Director of Engineering, Enablement:** [Accomplish Q1 hiring goals](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6202)
            1. Key result: Hire according to plan.
            1. Key result: Improve engineer interviewer ratio from 1:3.7 to 1:3.
            1. Key result: Introduce and hire at least 1 senior manager in Q1.
        1. **Director of Engineering, Growth:** [Accomplish Q1 hiring goals](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6238)
    1. **Director of Quality:** [Hire to plan and develop career matrix for ICs and Managers in the department](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6329)
        1. Key result: Hire according to plan.
        1. Key result: Provide a precise hiring order plan for the department in February.
        1. Key result: Develop Career Matrix for Software Engineers in Test and Quality Engineering Managers.
    1. **Director of Customer Support:** [Incorporate new manager layer and Support Operations team into organization](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6210)
        1. Key result: Clearly defined new Senior Manager role & responsibilities in Support and ensure smooth transition of reporting structure.
        1. Key result: Support Operations team is established with manager and initial specialists hired.
        1. Key result: Hiring plan is adjusted for new leadership and spans of control for Sr. Manager:Manager:IC.
    1. **Director of Customer Support:** [Make License & Renewal workflow seamless for customers](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6209)
        1. Key result: Clearly define existing workflows and processes to resolve customer tickets.
        1. Key result: Collaborate with Product on vision and prioritization of product enhancements.
        1. Key result: Work with Sales and Business Operations to determine appropriate team to transition work processes.
    1. **VP of Security:** [Implement new structure, introduce new director management layer](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6216)
        1. Key result: Implement the new Security department structure and reshuffle teams as required.
        1. Key result: Fill two of the new Director roles.
    1. **Interim VP of Infrastructure:** [Add layer of management due to headcount and ship stable counterparts to Engineering](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6378)
        1. Key result: Implement next iteration of Reliability stable counterparts
        1. Key result: Onboard VP, Director of SRE, and Sr/Mgr of Core & DBRE

## How to Achieve Presentations
